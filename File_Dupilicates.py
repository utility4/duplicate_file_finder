#!/usr/bin/env python
# coding: utf-8
#Untamed Yeti

import glob
import os
import hashlib
import pandas as pd
import shutil
import time

#function for hash values to get unique value of file
def md5(fname):
    hash_md5 = hashlib.md5() #can change to md5,sha1
    with open(fname, "rb") as f:
        for chunk in iter(lambda: f.read(4096), b""): #have no clue what the b for but it is needed.
            hash_md5.update(chunk)
    return hash_md5.hexdigest()

#update the file location
new_loc = r'C:\newfileloc'
duppath = new_loc + r'\Duplicates'
#check exist
if not os.path.exists(duppath):
    os.makedirs(duppath)
#this is the top file path where yor files are located
directory_path = r'C:\myfiles'

#empty data frames for temp list
results = []
hash_value_list = []
#walk through the dir and subdir
for root,d_names,f_names in os.walk(directory_path):
    for file in f_names:
        oldpath = os.path.join(root, file)
        newpath = os.path.join(new_loc, file)
        if (file.lower().endswith("pdf") or file.lower().endswith("txt")):
            #print('Analyzing: ' + oldpath)
            hash_value = md5(oldpath).upper()
            #xheck if hashvalue in list
            if hash_value in hash_value_list:
                newpath = os.path.join(duppath, file)
            else:
                next
            #add hash value to list
            hash_value_list.append(hash_value)
            
            i=1
            temp = newpath
            while os.path.isfile(newpath):
                newpath = temp
                newpath = newpath[:-4] + '_' + str(i) + newpath[-4:]
                i = i+1
            #this moves the files
            try:
                shutil.move(oldpath, newpath) #shutil.copyfile to copy file only
                print ('Moving: ' + oldpath + ' to ' + newpath) 
                #put the file in the results list
                results.append((file, hash_value, oldpath, newpath))
            except:
                print('Cannot move ' + file + 'from ' + 'this location: ' + oldpath)
#create a dataframe
df = pd.DataFrame(results, columns  = ['Filename', 'Hash_Value', 'Old_Loc', 'New_Loc']).sort_values('Hash_Value')        

unique_values = df.groupby('Hash_Value')['Filename'].unique()

# Create a Pandas Excel writer using XlsxWriter as the engine. This will show you what was moved
timestr = time.strftime("%Y%m%d-%H%M%S")
filename = new_loc + r'/results_' + timestr + r'.xlsx'
writer = pd.ExcelWriter(filename, engine='xlsxwriter')
# Write each dataframe to a different worksheet.
df.to_excel(writer, sheet_name='Sheet1', index = False)
unique_values.to_excel(writer, sheet_name='Sheet2')
# Close the Pandas Excel writer and output the Excel file.
writer.save()

print('Done')
